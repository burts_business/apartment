<?php /* Template Name: Main */
get_header(); ?>
   

	<div id="about-us">   
		    
		<img class="header-image" src="<?php the_field('header_image'); ?>" alt="title" />
		<section class="cd-section lead clear">
			<?php if (have_posts()) : 
				while (have_posts()) : the_post(); ?>   
				<h1><?php the_title(); ?></h1>
				<?php the_content(); 	
				endwhile; 
				endif; 
				
				 if ( is_page( 'Contact Us' ) ) {
				
				 echo do_shortcode('[contact-form-7 id="15" title="Contact form 1"]'); 
				
				 } else {
			  //everything else ?>
			  
			  	<div class="half">
					<?php the_field('first_half'); ?>
				</div>
				<div class="half">
					<?php the_field('second_half'); ?>
				</div>
			</section>
			          
   
			<section class="cd-section lead clear">
				<h2>See what we have available</h3>
				<?php $args = array( 'post_type' => 'post', 'posts_per_page' => 999, 'order' => 'DESC' );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
						
						 <div class="third">
					            <a href="<?php the_permalink();?>">
									<?php if ( has_post_thumbnail() ) { 
										the_post_thumbnail('header'); 
									}?>
									<h3><?php the_title();?></h3>
								</a>
				            </div>      
						
						<?php endwhile; ?>
			</section>
	<?php	}?>
		
	
		
	</section>


				
<?php get_footer(); ?>