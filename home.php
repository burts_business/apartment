<?php get_header(); ?>

    
    <section class="cd-section clear">
	    <div class="third">
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-storage.svg" alt="clear out your garage" />
		    <h3>Clear out your garage</h3>
		    <p>Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. At cursus commodo.</p>
	    </div>
	    <div class="third">
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-tree.svg" alt="clear out your garage" />
		    <h3>Clear out your garage</h3>
		    <p>Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. At cursus commodo.</p>
	    </div>
	    <div class="third">
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-money.svg" alt="clear out your garage" />
		    <h3>Clear out your garage</h3>
		    <p>Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. At cursus commodo.</p>
	    </div>
    </section>
    
    <div class="clear white">
	    <section class="cd-section lead">
	    	<h2>It couldn't be easier</h2>
	    	<p>With no need for building approval, permits or any unnecessary fluffing about, Attic Installations can solve your storage solutions today. So why wait? </p>
	    	<a href="#" class="button">Get a free quote</a>
	    </section>
    </div>
    
    <section class="cd-section lead clear">
    	<h2>Examples of our work</h2>
    	<p>Take a look at the discreet, professional installs will look like 
in your home.</p>
    	<a href="#" class="button">Our work</a>
    </section>
    
    <div class="clear">
	    <section class="cd-section lead">
	    	<h2>We're experienced</h2>
	    	<p>Fully qualified professional builders with upwards of 12 years of experience. We are customer focused and offer full satisfaction. Find out more about us below.</p>
	    	<a href="#" class="button">About us</a>
	    </section>
    </div>
    
    
<?php get_footer(); ?>