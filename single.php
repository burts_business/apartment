<?php get_header(); ?>

<div id="single-posts">
	<div class="clear">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>  
				<section id="intro" class="clear">
					<?php the_post_thumbnail('featured-large'); ?>
					<div id="intro-caption" class="title-section">
						<h1><?php the_title(); ?></h1><h1><span class="comma">,</span> <?php the_field('general_location'); ?></h1>
						<h3 class="price"><?php the_field('price_range'); ?></h3>
						<a href="#" class="button">Get More Info</a>
				    </div>
				</section>
				<section class="cd-section lead" style="padding-bottom: 0px;">
			    	<?php the_content(); ?>
			    </section>
			    <img class="line" src="<?php bloginfo('stylesheet_directory'); ?>/images/line-09.svg" alt="line" />
			    <section class="cd-section lead apartment-options clear">
			    	<h2>Apartment Options</h2>
			    	<div id="main-slider">
				    	
			          <ul class="slies">
				        <?php if( have_rows('apartment_options') ):
  						while ( have_rows('apartment_options') ) : the_row();?>
				          <li>
					    	<div class="half">
						    	<img src="<?php the_sub_field('floor_plan'); ?>" alt="yep" />
					    	</div>
					    	<div class="half">
						    	<h3 class="price"><?php the_sub_field('details'); ?></h3>
						    	<h4><?php the_sub_field('detail_price'); ?></h4>
						    	<p><?php the_sub_field('blurb'); ?></p>
						    	<div class="apartment-details">
						    		<div class="detail">
							    		<h4>Bedrooms</h4>
							    		<h5><?php the_sub_field('bedrooms'); ?></h5>
						    		</div>
						    		<div class="detail">
							    		<h4>Bathrooms</h4>
							    		<h5><?php the_sub_field('bathrooms'); ?></h5>
						    		</div>
						    		<div class="detail">
							    		<h4>Floor Size</h4>
							    		<h5><?php the_sub_field('size'); ?></h5>
							    		<p>sqm.</p>
						    		</div>
						    		
						    	</div>
						    	<a href="#" class="button">Get More Info</a>
					    	</div>
				          </li>
				          <img class="line" src="<?php bloginfo('stylesheet_directory'); ?>/images/line-09.svg" alt="line" />

				          <?php endwhile;
				          else :
				          endif;?>
				         
			          </ul>
			          
			    	</div>
				    	
			    	
			    </section>
			    <section class="cd-section lead gallery clear">
				    <?php 
					$images = get_field('gallery');
					if( $images ): ?>
					    <ul>
					        <?php foreach( $images as $image ): ?>
					            <li>
					                <a href="<?php echo $image['url']; ?>">
					                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					                </a>
					            </li>
					        <?php endforeach; ?>
					    </ul>
					<?php endif; ?>
			    </section>
			    <section class="cd-section maps">
			    	<?php the_field('map'); ?>
			    </section>
				 <section class="cd-section lead clear">
			    	<h2>Limited availabiltiy. <br>
					Don’t miss out</h2>
			    	<p>Like all Off the Plan Apartments, this won’t last long. Don’t be one of the many who hesitate and will miss this opportunity. </p>
			    	<a href="#" class="button">Apply for Info Pack</a>
			    </section>
			
			
			
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>	
		
<?php get_footer(); ?>