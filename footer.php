			<footer class="cd-section clear white">
		        <div class="third left">
			        <img class="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt-"footer logo" />
		        </div>
		        <div class="third right clear">
			       <p>Contact us Today</p>
			       <a href="#" class="button">Contact Us</a>
			    </div>
				<section class="contact-bar bottom cd-section">
					<p>&#169; All Rights Reserved - <?php bloginfo('name'); ?> <?php echo date("Y") ?>. Licenced REAA 2008.</p>
					<a class="blink" href="http://blinkltd.co.nz/" target="_blank"> | handmade by blink.</a>
					<a href="http://blinkltd.co.nz/" target="_blank">Powered by Ray White&nbsp;</a>
				</section> 
			</footer>		        

			
		</div><!--main content-->
		
	
	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->


	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300' rel='stylesheet' type='text/css'>
	
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
	<script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider.js"></script>
	
	<script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('#main-slider').flexslider({
        animation: "slide",
        slideshow: false,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
    </script>

  
<?php wp_footer(); ?>


</body>
</html>