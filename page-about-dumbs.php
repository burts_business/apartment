<?php get_header(); /* Template Name: Main */
	 if (have_posts()) :
	 while (have_posts()) : the_post(); ?>    
		
		<div id="about-us">        
			<img class="header-image" src="<?php bloginfo('stylesheet_directory'); ?>/images/title.jpg" alt="title" />
			<section class="cd-section lead clear">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<div class="half">
					<h3>About Sam Woods</h3>
					<p>Sed posuere consectetur est at lobortis. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam quis risus eget urna mollis ornare vel eu leo. Donec sed odio dui. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
				</div>
				<div class="half">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/sam.jpg" alt="Sam Woods" />
				</div>
			</section>
			<section class="cd-section lead clear">
				<h2>See what we have available</h3>
				<div class="third">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/eg2.jpg" alt="" />
					<h3>Parkview Residences</h3>
				</div>
				<div class="third">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/eg2.jpg" alt="" />
					<h3>Parkview Residences</h3>
				</div>
				<div class="third">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/eg2.jpg" alt="" />
					<h3>Parkview Residences</h3>
				</div>
			</section>
		</div>

    <?php endwhile; 
    endif; 
 get_footer(); ?>