<?php get_header(); ?>
    
    <section class="cd-section clear">
	    <?php $args = array( 'post_type' => 'post', 'posts_per_page' => 999, 'order' => 'DESC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
		
		 <div class="listing clear">
			<?php the_post_thumbnail('featured-small'); ?>
			<div class="listing-text">
				<h2><?php the_title(); ?></h2>
				<h3><?php the_field('general_location'); ?></h3>
				<h3 class="price"><?php the_field('price_range'); ?></h3>
				<h3><?php the_field('general_bedrooms'); ?></h3>
				<p class="explination">Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod.</p>
				<a href="<?php the_permalink(); ?>" class="button button-rev">Find Out More</a>
			</div>
		</div>   
		
		<?php endwhile; ?>
		
		
		<div class="listing clear">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/eg2.jpg" alt="image" />
			<div class="listing-text">
				<h2>The James Residences</h2>
				<h3>Eden Terrace</h3>
				<h3 class="price">$350,000 - $1,700,000</h3>
				<h3>1 - 2 Bedroom Apartments</h3>
				<p class="explination">Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod.</p>
				<a href="#" class="button button-rev">Find Out More</a>
			</div>
		</div>
		
		<div class="listing clear">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/eg3.jpg" alt="image" />
			<div class="listing-text">
				<h2>Lakewood Plaza</h2>
				<h3>Eden Terrace</h3>
				<h3 class="price">$350,000 - $1,700,000</h3>
				<h3>1 - 2 Bedroom Apartments</h3>
				<p class="explination">Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod.</p>
				<a href="#" class="button button-rev">Find Out More</a>
			</div>
		</div>
	
	</section>
    
    <!--<div class="clear white">
	    <section class="cd-section lead">
	    	<h2>It couldn't be easier</h2>
	    	<p>With an emphasis on quality and value for money, Attic Installations can solve your home storage problems today. So why wait?</p>
	    	<a href="tel:+64 9 212 9970" class="button">Call us today</a>
	    </section>
    </div>-->
    

    
    
<?php get_footer(); ?>