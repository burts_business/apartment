<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php bloginfo('name'); ?></title>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" />
    <!--<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/faq.css" />-->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" />
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.3.2/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />

	 <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" type="text/css" media="screen" />
   	<link rel="icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />
   	<link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />

 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 	<!--<script src="<?php bloginfo('stylesheet_directory'); ?>/js/faq.js"></script>-->
 	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>
 	
 	<script src="https://use.typekit.net/gah8hly.js"></script>
 	<script>try{Typekit.load({ async: true });}catch(e){}</script>

		
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	 <div class="inner-wrap"> 
		

		<div id="main-content" class="clear" role="document">
			
			<?php if ( is_page( 'Homepage' ) ){ ?>
				
				
					<section>
						<header class="header-home clear">	
							<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" /></a>
							<?php echo do_shortcode('[google-translator]'); ?>
				        	<nav>
					        	<ul class="desktop">
									<li><a href="<?php echo home_url(); ?>">Our Listings</a></li>
									<li><a href="<?php echo home_url(); ?>/why/">Why off the plan?</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
					        	</ul>
					        	
								<ul class="mob-nav">
									<li><a href="<?php echo home_url(); ?>">Home</a></li>
									<li><a href="<?php echo home_url(); ?>">Our Listings</a></li>
									<li><a href="<?php echo home_url(); ?>/why/">Why off the plan?</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
								</ul>
								<div class="mobile right mob-logo">
									<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="menu" />
								</div>
							</nav> 
					    </header>				
					     
					</section>
					<section id="intro" class="clear">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/intro.jpg" alt="intro" />
						<div id="intro-caption">
							<div class="overlay">
								<h1>Alexander Park Residences</h1>
								<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam quis risus eget urna mollis ornare vel eu leo. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum.</p>
								<a href="#" class="button">Find Out More</a>
							</div>
					    </div>
					</section>
			
			<?php }
				else 
			{?>
			
			<section>
				<header class="header-home clear internal">	
							<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" /></a>
							<?php echo do_shortcode('[google-translator]'); ?>
				        	<nav>
					        	<ul class="desktop">
									<li><a href="<?php echo home_url(); ?>">Our Listings</a></li>
									<li><a href="<?php echo home_url(); ?>/why/">Why off the plan?</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
					        	</ul>
					        	
								<ul class="mob-nav">
									<li><a href="<?php echo home_url(); ?>">Home</a></li>
									<li><a href="<?php echo home_url(); ?>">Our Listings</a></li>
									<li><a href="<?php echo home_url(); ?>/why/">Why off the plan?</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
								</ul>
								<div class="mobile right mob-logo">
									<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="menu" />
								</div>
							</nav> 
					    </header>				
			     
			</section>
			  			
			<?php }
			?>

			
			
	
			